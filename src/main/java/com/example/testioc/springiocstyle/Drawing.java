package com.example.testioc.springiocstyle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "prototype")
public class Drawing {

    private String name;

    private Shape shape;

    @Autowired
    public Drawing(@Qualifier("circle") Shape shape) {
        this.shape = shape;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //Field Injection
    /*@Autowired
    @Qualifier("circle")
    private Shape shape;*/

    // Setter Injection
   /* @Autowired
    @Qualifier("triangle")
    public void setShape(Shape shape) {
        this.shape = shape;
    }*/

    public void drawShape() {
        this.shape.draw();
    }
}
