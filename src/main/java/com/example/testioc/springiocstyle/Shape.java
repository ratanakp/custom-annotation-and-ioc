package com.example.testioc.springiocstyle;

public interface Shape {
    void draw();
}
