package com.example.testioc.springiocstyle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

//    @Bean
//    public Drawing drawing() {
//        Drawing draw = new Drawing();
//        draw.setShape(new Circle());
//        return draw;
//    }

    @Bean
    public Circle circle() {
        return new Circle();
    }

    @Bean
    public Triangle triangle() {
        return new Triangle();
    }

}
