package com.example.testioc.customannotation;

import java.lang.annotation.Annotation;

public class MainClass {
    public static void main(String[] args) {
        NokiaASeries aSeries = new NokiaASeries("Nokia 6");

        Class c = aSeries.getClass();
        Annotation annotation = c.getAnnotation(SmartPhone.class);

        SmartPhone smartPhoneAnnotation = (SmartPhone) annotation;

        System.out.println("Name: "+aSeries.getName());
        System.out.println("OS: " + smartPhoneAnnotation.OS());
        System.out.println("Version: " + smartPhoneAnnotation.version());

        System.out.println(c);
        System.out.println(smartPhoneAnnotation);
    }
}
