package com.example.testioc.customannotation;

// Marker Annotation
// Single Annotation
// Multi Annotation

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SmartPhone {
    String OS();
    double version() default 1.0;
}
