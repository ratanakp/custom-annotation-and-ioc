package com.example.testioc.customannotation;

@SmartPhone(OS = "Android", version = 7.0)
public class NokiaASeries {

    private String name;

    public NokiaASeries() {
    }

    public NokiaASeries(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
