package com.example.testioc;

import com.example.testioc.springiocstyle.Drawing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class TestIocApplication {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(TestIocApplication.class, args);
		Drawing drawing = context.getBean("drawing", Drawing.class);
		drawing.setName("Circle!");
//		drawing.drawShape();

		Drawing drawing1 = context.getBean("drawing", Drawing.class);
		drawing1.setName("Triangle!");

		System.out.println(drawing.getName());
		System.out.println(drawing1.getName());

	}
}
