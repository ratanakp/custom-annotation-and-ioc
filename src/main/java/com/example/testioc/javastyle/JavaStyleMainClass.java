package com.example.testioc.javastyle;

public class JavaStyleMainClass {

    public static void main(String[] args) {
        Shape shape = new Circle();

        shape.draw();

        Shape shape1 = new Triangle();
        shape1.draw();
    }
}
