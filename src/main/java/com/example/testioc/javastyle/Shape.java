package com.example.testioc.javastyle;

public interface Shape {
    void draw();
}
