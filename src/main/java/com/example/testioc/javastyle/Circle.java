package com.example.testioc.javastyle;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Draw Circle!");
    }
}
